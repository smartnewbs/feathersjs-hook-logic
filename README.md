
# @smartnewbs/feathersjs-hook-logic
![License](https://img.shields.io/npm/l/@smartnewbs/feathersjs-hook-logic.svg)
![Version](https://img.shields.io/npm/v/@smartnewbs/feathersjs-hook-logic.svg)
![Node](https://img.shields.io/node/v/@smartnewbs/feathersjs-hook-logic.svg)
[![Coverage Status](https://coveralls.io/repos/bitbucket/smartnewbs/feathersjs-hook-logic/badge.svg?branch=master)](https://coveralls.io/bitbucket/smartnewbs/feathersjs-hook-logic?branch=master)

This lib is for [Feathersjs](https://github.com/feathersjs/feathers) as an after hook. 
It is dependent on the [stashBefore](https://docs.feathersjs.com/guides/step-by-step/basic-feathers/writing-hooks.html#stashbefore-source)
hook which is part of the official Feathersjs [common hook](https://github.com/feathers-plus/feathers-hooks-common) library. 

The goal of this hook is monitor changes on service models for specific changes and trigger a business logic. 
The hook can only be an **after hook** with the **stashBefore** hooked in the same method before hook.

## Installation
```
npm install @smartnewbs/featherjs-hook-logic --save
```

## Defining a business logic for a service
Actions will receive three arguments. The first one is the Featherjs app (_hook.app_), the second
will be the original model (provided by _stashBefore_) and the third will be the requested data change
(_hook.data_)

If you wanted to send an email when a user changes his email as a warning all you need to do
is create a new file `src/services/users/users.logic.js` with the following:
```javascript
const {hook} = require('@smartnewbs/feathersjs-hook-logic');

module.exports = function () {
  const logic = {
    email: [
      {action: sendEmailWarning}
    ]
  };

  function sendEmailWarning(app, original, changed) {
    // Send email to user
  }

  return hook(logic);
};
```

The logic object structure reflects the property of the model and its value is an array of triggers
which has three properties: **old**, **new** and **action**

If only the action is specified it will trigger the action regardless of the change that is being made.

Let say you want to send a welcome email when a user has verified his email, 
which changes his status from **STATUS_NEW** to **STATUS_ACTIVE**. Your hook would look like:
```javascript
const {hook} = require('@smartnewbs/feathersjs-hook-logic');

module.exports = function () {
  const logic = {
    email: [
      {old: 'STATUS_NEW', new: 'STATUS_ACTIVE', action: sendWelcomEmail}
    ]
  };

  function sendWelcomEmail(app, original, changed) {
    // Send email to user
  }

  return hook(logic);
};
```
The properties **old** and **new** do not require each other, if they are present
they will be verified against _stashBefore_ for **old** and _hook.data_ for **new**.

## Hooking the logic to your service
Usually the `users.hooks.js` should reside in the same directory as your `users.logic.js`. 
So in your `users.hooks.js` just add the following:
```javascript

const {stashBefore} = require('feathers-hooks-common');
const logic = require('./users.logic');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [ stashBefore() ],
    update: [ stashBefore() ],
    patch:  [ stashBefore() ],
    remove: [ stashBefore() ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [ logic() ],
    update: [ logic() ],
    patch:  [ logic() ],
    remove: [ logic() ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

```


