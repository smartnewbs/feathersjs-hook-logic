
const assert = require('assert');
const sinon = require('sinon');
const lib = require('../lib/');
const hook = lib.hook;

let afterHook = {};
let featherjsApp = {featherjs: 'app'};

describe('Logic Hook', () => {
  before(() => {
    sinon.spy(lib, 'compareChanges');
    sinon.spy(lib, 'triggerLogicChanges');
  });

  beforeEach(() => {
    lib.compareChanges.reset();
    lib.triggerLogicChanges.reset();

    afterHook = {
      app: featherjsApp,
      type: 'after',
      method: 'update',
      params: {
        before: { status: 'STATUS_NEW', role: 'ROLE_SUPPORT', email: 'test1' },
      },
      data: { status: 'STATUS_ACTIVE', role: 'ROLE_ADMIN', email: 'test2' },
      id: null,
    };
  });

  it('Must throw error if in a before hook', () => {
    afterHook.type = 'before';

    assert.throws(() => {
      hook({})(afterHook);
    }, lib.error.HookLogic);
  });

  it('Must throw error if commonHooks.stashBefore() is not applied', () => {
    delete afterHook.params.before;

    assert.throws(() => {
      hook({})(afterHook);
    }, lib.error.HookLogic);
  });

  it('Must do nothing if no data is provided', () => {
    delete afterHook.data;

    hook({})(afterHook);
    assert.equal(lib.compareChanges.called, false, 'compareChanges should not of been called');
    assert.equal(lib.triggerLogicChanges.called, false, 'triggerLogicChanges should not of been called');
  });

  it('Must do nothing if no changes', () => {
    afterHook.params.before.status = 'STATUS_ACTIVE';
    afterHook.params.before.role = 'ROLE_SUPPORT';
    afterHook.params.before.email = 'test';
    afterHook.data.status = 'STATUS_ACTIVE';
    afterHook.data.role = 'ROLE_SUPPORT';
    afterHook.data.email = 'test';

    hook({})(afterHook);
    assert.equal(lib.compareChanges.called, true, 'compareChanges should of been called');
    assert.equal(lib.triggerLogicChanges.called, false, 'triggerLogicChanges should not of been called');
  });

  it('Must track changes properly', () => {
    afterHook.params.before.email = 'test';
    afterHook.data.email = 'test';
    afterHook.data.test = 'test';

    const results = lib.compareChanges(afterHook.params.before, afterHook.data);
    const fieldNames = results.map(item => {
      return item.name;
    });

    assert.notEqual(fieldNames.indexOf('status'), -1);
    assert.notEqual(fieldNames.indexOf('role'), -1);
    assert.equal(fieldNames.indexOf('email'), -1);
    assert.equal(fieldNames.indexOf('test'), -1);
  });

  it('triggerLogicChanges() must receive proper arguments', () => {
    const logic = {test: 'some logic'};

    hook(logic)(afterHook);

    const compareSpyCalls = lib.compareChanges.getCalls();
    const triggerSpyCalls = lib.triggerLogicChanges.getCalls();
    for (let i=0; i < triggerSpyCalls.length; i++) {
      const triggerSpy = triggerSpyCalls[i];
      const compareSpy = compareSpyCalls[i];

      assert.equal(triggerSpy.args.length, 3, 'triggerLogicChanges() should of receive 3 arguments');
      assert.deepEqual(triggerSpy.args[0], featherjsApp, 'First argument should be the Featherjs app');
      assert.deepEqual(triggerSpy.args[1], logic, 'Second argument should be the logic object');
      assert.deepEqual(triggerSpy.args[2], compareSpy.returnValue, 'Third argument should be the array of changes');
    }
  });

  it('Actions must be called with proper arguments', () => {
    const actionSpy = sinon.spy();
    const logic = {
      email: [ {action: actionSpy} ],
      status: [ {action: actionSpy, new: 'STATUS_ACTIVE'} ],
      role: [ {action: actionSpy, old: 'STATUS_SUPPORT', new: 'STATUS_ACTIVE'} ],
    };

    hook(logic)(afterHook);

    const actionSpyCalls = actionSpy.getCalls();
    for (let i=0; i < actionSpyCalls.length; i++) {
      const actionSpy = actionSpyCalls[i];

      assert.equal(actionSpy.args.length, 3, 'Action should of receive 3 arguments');
      assert.deepEqual(actionSpy.args[0], featherjsApp, 'First argument should be the Featherjs app');
      assert.deepEqual(actionSpy.args[1], afterHook.params.before, 'Second argument should be the original entry (stashBefore)');
      assert.deepEqual(actionSpy.args[2], afterHook.data, 'Third argument should be the array of changes');
    }

  });

  describe('Must trigger the right logic functions', () => {

    it('Trigger actions with no "old" or "new" property', () => {

      const fieldChanges = [
        {name: 'email', original: afterHook.params.before, changed: afterHook.data}
      ];

      const emailAction = sinon.spy();
      const logic = {
        status: [ {action: emailAction } ],
        email: [
          { action: emailAction },
          { action: emailAction },
          { action: emailAction }
        ]
      };

      lib.triggerLogicChanges(featherjsApp, logic, fieldChanges);

      assert.equal(emailAction.called, true);
      assert.equal(emailAction.callCount, 3);
    });

    it('Trigger actions with "new" property', () => {

      const fieldChanges = [
        {name: 'status', original: afterHook.params.before, changed: afterHook.data}
      ];

      const statusAction = sinon.spy();
      const logic = {
        email: [ { action: statusAction, new: 'test'} ],
        status: [
          { action: statusAction, new: 'STATUS_INVALID' },
          { action: statusAction, new: 'STATUS_ACTIVE' },
          { action: statusAction, new: 'STATUS_ACTIVE' },
          { action: statusAction }
        ]
      };

      lib.triggerLogicChanges(featherjsApp, logic, fieldChanges);

      assert.equal(statusAction.called, true);
      assert.equal(statusAction.callCount, 3);
    });

    it('Trigger actions with "new" & "old" property', () => {

      const fieldChanges = [
        {name: 'status', original: afterHook.params.before, changed: afterHook.data}
      ];

      const statusAction = sinon.spy();
      const logic = {
        email: [ { action: statusAction, new: 'test'} ],
        status: [
          { action: statusAction, old: 'STATUS_TEST', new: 'STATUS_INVALID' },
          { action: statusAction, old: 'STATUS_NEW', new: 'STATUS_ACTIVE' },
          { action: statusAction, old: 'STATUS_NEW', new: 'STATUS_ACTIVE' },
          { action: statusAction, new: 'STATUS_ACTIVE' },
          { action: statusAction }
        ]
      };

      lib.triggerLogicChanges(featherjsApp, logic, fieldChanges);

      assert.equal(statusAction.called, true);
      assert.equal(statusAction.callCount, 4);
    });

    it('Trigger actions are not called if no matches or changes', () => {
      afterHook.params.before.status = 'STATUS_ACTIVE';
      afterHook.params.before.role = 'ROLE_SUPPORT';
      afterHook.params.before.email = 'test';
      afterHook.data.status = 'STATUS_ACTIVE';
      afterHook.data.role = 'ROLE_SUPPORT';
      afterHook.data.email = 'test';

      const fieldChanges = [
        {name: 'status', original: afterHook.params.before, changed: afterHook.data}
      ];

      const statusAction = sinon.spy();
      const logic = {
        email: [ { action: statusAction, new: 'test'} ],
        status: [
          { action: statusAction, old: 'STATUS_NEW', new: 'STATUS_ACTIVE' },
          { action: statusAction, new: 'STATUS_ACTIVE' },
          { action: statusAction }
        ]
      };

      lib.triggerLogicChanges(featherjsApp, logic, fieldChanges);

      assert.equal(statusAction.called, false);
      assert.equal(statusAction.callCount, 0);
    });

    it('Does not trigger actions if there is no action...', () => {
      const statusAction = sinon.spy();
      const fieldChanges = [
        {name: 'status', original: afterHook.params.before, changed: afterHook.data},
        {name: 'email', original: afterHook.params.before, changed: afterHook.data}
      ];

      const logic = {
        status: [ { action: statusAction } ],
        email: [ { new: 'test2' } ]
      };

      lib.triggerLogicChanges(featherjsApp, logic, fieldChanges);

      assert.equal(statusAction.called, true);
      assert.equal(statusAction.callCount, 1);
    });

  });
});

