
const error = require('./error');
exports.error = error;

/**
 * Returns the hook
 *
 * @param logic
 * @returns {function(*)}
 */
exports.hook = function hook(logic) {
  return hook => {

    if (hook.data === undefined) {
      return hook;
    }

    if (hook.type !== 'after') {
      throw new error.HookLogic('@smartnewbs/feathersjs-hook-logic can only be used as an after hook');
    }

    // if the stashBefore is not hooked we stop here
    if (!hook.params.hasOwnProperty('before')) {
      throw new error.HookLogic('@smartnewbs/feathersjs-hook-logic requires the commonHooks.stashBefore() to be hooked in your before model hooks');
    }

    // if we have no changes to trigger then we stop here
    const original = hook.params.before;
    const fieldChanges = exports.compareChanges(original, hook.data);
    if (fieldChanges.length === 0) {
      return hook;
    }

    exports.triggerLogicChanges(hook.app, logic, fieldChanges);

    return hook;
  };

};

/**
 * Compares changes between original model and changed model
 *
 * @param original
 * @param changed
 * @returns {Array}
 */
exports.compareChanges = function compareChanges(original, changed) {
  const changes = [];

  for (const name in changed) {
    if (!changed.hasOwnProperty(name) || !original.hasOwnProperty(name)) {
      continue;
    }

    const newValue = changed[name];
    const oldValue = original[name];

    if (newValue !== oldValue) {
      changes.push({
        name,
        original,
        changed
      });
    }
  }

  return changes;
};

/**
 * Triggers the actions of the service if any
 *
 * @param app # Featherjs app
 * @param logic
 * @param fieldChanges
 */
exports.triggerLogicChanges = function triggerLogicChanges(app, logic, fieldChanges) {

  for (let i = 0; i < fieldChanges.length; i++) {
    const change = fieldChanges[i];
    if (!logic.hasOwnProperty(change.name)) {
      continue;
    }

    const triggers = logic[change.name];
    for (let j = 0; j < triggers.length; j++) {

      const trigger = triggers[j];
      if (!trigger.hasOwnProperty('action')) {
        continue;
      }

      const oldValue = change.original[change.name];
      const newValue = change.changed[change.name];

      // if the action has a property "old" or "new" they need to match
      if ((trigger.hasOwnProperty('old') && trigger.old !== oldValue)
        || (trigger.hasOwnProperty('new') && trigger.new !== newValue)) {

        continue;
      }

      // if there was no change in value we skip
      if (oldValue === newValue) {

        continue;
      }

      trigger.action(app, change.original, change.changed);
    }
  }
};
