
class Base extends Error {

  constructor(message, data = {}) {
    super();

    this.name = this.constructor.name;
    this.message = message;
    this.data = data;
  }

}

module.exports = Base;
